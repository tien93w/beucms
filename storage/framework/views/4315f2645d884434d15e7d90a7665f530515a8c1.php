<?php ?>


<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
            <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                          <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                      <h3 class="m-portlet__head-text">
                                            Tạo mới sản phẩm
                                      </h3>
                                </div>
                          </div>
                          <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                      <li class="m-portlet__nav-item">
                                                            </li>
                                </ul>
                          </div>
                          
                    </div>
                    <div class="m-portlet__body">
    
                          <!-- Display Validation Errors -->
                        <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
                    </div>
                    <form method="post" action="<?php echo e(route('product.store')); ?>" enctype='multipart/form-data'>
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                            <label for="title" class="col-md-12 control-label">Tên sản phẩm</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title" value="<?php echo e(old('title')); ?>" onkeyup="ChangeToSlug()"
                                       required autofocus>

                                <?php if($errors->has('title')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('title')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="thumbnail" class="col-md-5 control-label">Thumbnail</label>
                            <span class="form-group-btn">
                              <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary text-white">
                                <i class="fa fa-picture-o"></i> Chọn
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="thumbnail" value="<?php echo e(old('thumbnail')); ?>">
                            <img id="holder" style="margin-top:15px;max-height:100px;" src="">
                          </div>


                        <div class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                            <label for="slug" class="col-md-12 control-label">Slug sản phẩm</label>

                            <div class="col-md-12">
                                <input id="slug" type="text" class="form-control" name="slug" value="<?php echo e(old('slug')); ?>"
                                       >

                                <?php if($errors->has('slug')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('slug')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                            <label for="price" class="col-md-12 control-label">Gía sản phầm</label>

                            <div class="col-md-12">
                                <input id="price"  type="text" class="form-control" name="price" value="<?php echo e(old('price')); ?>"
                                       >

                                <?php if($errors->has('price')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('price')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                          <label for="">Danh muc</label>
                          <select name="productcate_id" class="form-control" id="productcate_id">
                              <option value="0">Chọn</option>
                              <?php categoryParent($data)?>
                          </select>
                      </div>


                      <div class="form-group col-md-12">
                        <label for="">Sản phẩm nổi bật</label>
                        <select name="featured" class="form-control" id="featured">
                            <option value="">Chọn</option>
                            <option value="1">Có</option>
                            <option value="0">Không</option>
                        </select>
                    </div>
                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-12 control-label">Mô tả ngắn</label>

                            <div class="col-md-12">
                                          <textarea id="description" name="description" class="form-control my-editor"><?php echo e(old('description')); ?></textarea>
                                <?php if($errors->has('description')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('description')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('body') ? ' has-error' : ''); ?>">
                            <label for="body" class="col-md-12 control-label">Nội Dung</label>

                            <div class="col-md-12">
                                          <textarea id="body" name="body" rows="35" class="form-control my-editor"><?php echo e(old('body')); ?></textarea>
                                <?php if($errors->has('body')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('body')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="dropzone" id="myDropzone"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="submit-all">
                                    Tạo mới
                                </button>

                                <a class="btn btn-link" href="<?php echo e(route('product.index')); ?>">
                                    Hủy
                                </a>
                            </div>
                        </div>
                    </form>
                    <script>
                        
                    </script>
              </div>
    </div>
  

    <script src="<?php echo e(asset('/vendor/laravel-filemanager/js/lfm.js')); ?>"></script>
    
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script type="text/javascript">
    $('#lfm').filemanager('image');
    function ChangeToSlug()
            {
                var title, slug;
    
                //Lấy text từ thẻ input title
                title = document.getElementById("title").value;
    
                //Đổi chữ hoa thành chữ thường
                slug = title.toLowerCase();
    
                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                //In slug ra textbox có id “slug”
                // var url = '<?php echo e(url('/page/')); ?>';
                // document.getElementById('link').value = url +'/'+ slug;
                document.getElementById('slug').value = slug;
    
            }
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }
    
      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
    };
    
    tinymce.init(editor_config);
    
    
    </script>
        </div>
        <style>
            .dropzone {
                border: 2px dashed #0087F7;
                border-radius: 5px;
                background: white;
            }
        </style>
    
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('js'); ?>
    <script type="text/javascript">
        Dropzone.options.myDropzone = {
                        //url does not has to be written 
                        //if we have wrote action in the form 
                        //tag but i have mentioned here just for convenience sake 
                            url: route('product.store'), 
                            autoProcessQueue: false,
                            headers: {
               'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
           },
    uploadMultiple: true,
    parallelUploads: 10,
    maxFiles: 5,
    maxFilesize: 1,
    acceptedFiles: 'image/*',
    addRemoveLinks: true,
    init: function() {
        dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

        // for Dropzone to process the queue (instead of default form behavior):
        document.getElementById("submit-all").addEventListener("click", function(e) {
            // Make sure that the form isn't actually being sent.
            //e.preventDefault();
            e.stopPropagation();
            dzClosure.processQueue();
        });

        //send all the form data along with the files:
        this.on("sendingmultiple", function(data, xhr, formData) {
            // formData.append("title", jQuery("#title").val());
            // formData.append("price", jQuery("#price").val());
            // formData.append("slug", jQuery("#slug").val());
            // formData.append("thumbnail", jQuery("#thumbnail").val());
            // formData.append("productcate_id", jQuery("#productcate_id").val());
            // formData.append("featured", jQuery("#featured").val());
            formData.append("description2", $("#description").val());
            formData.append("body2", $("#body").val());
        });
    }
                        };
       
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\beucms\resources\views/admin/product/create.blade.php ENDPATH**/ ?>