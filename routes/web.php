<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::group(['prefix' => 'quan-tri','middleware' => 'auth'], function() {

    Route::get('/', 'HomeController@index')->name('admin');
    //menu
    Route::get('/menu','Admin\MenuController@menurender')->name('menu');
    // user
    Route::resource('user','Admin\UserController');
    // role
    Route::resource('roles', 'Admin\RoleController');
    //Page
    Route::resource('page', 'Admin\PageController');
    Route::resource('postcate', 'Admin\PostCategoryController');
    Route::resource('post', 'Admin\PostController');
    Route::resource('page', 'Admin\PageController');
    Route::resource('productcate', 'Admin\ProductCategoryController');
    Route::resource('product', 'Admin\ProductController');
    Route::post('uploadImg', 'Admin\ImageController@postImages')->name('adminimg'); 
    Route::post('deleteImg', 'Admin\ImageController@delImages')->name('adminimgdel'); 
    //File manager
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});