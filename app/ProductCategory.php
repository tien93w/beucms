<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
  protected $fillable = [
      'title', 'slug_id', 'description',
  ];

  public function slugs()
  {
      return $this->hasOne('App\Slug','id','slug_id');
  }

  public function parent(){

    return $this->belongsTo('App\ProductCategory','parent_id');

  }
}
