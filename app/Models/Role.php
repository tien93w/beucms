<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use App\User;
use App\Models\Permission;
class Role extends EntrustRole
{
    protected $fillable = [
        'name', 'display_name', 'description',
    ];
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * The role's users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
