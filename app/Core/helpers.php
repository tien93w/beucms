<?php

function showMenus($data, $parent_id = 0, $char = 'dropdown-menu', $drop = 'dropdown-toggle', $togel = 'dropdown')
{
      $pa =[];
      foreach ($data as $key => $value) {
        array_push($pa,$value['parent']);
      }
      foreach ($data as $item)
      {
          if ($item['parent'] == $parent_id && in_array($item['id'],$pa) ) {
            if ($item['depth']>=1) {
              echo '<li   class="dropdown-submenu">';
            }else {
              echo '<li   class="">';
            }
            echo '<a href="' . $item['link'] . '" class="'.$drop.'" data-toggle="'.$togel.'">' . $item['label'] . '</a>';
            echo '<ul  class="'.$char.'">';
            showMenus($data, $item['id'], $char='dropdown-menu', $drop='dropdown-toggle', $togel='dropdown');
            echo '</ul>';
            echo '</li>';
          }else if($item['parent'] == $parent_id) {
            echo '<li>';
            echo '<a href="' . $item['link'] . '">' . $item['label'] . '</a>';
            echo '</li>';
          }

      }

}

function showMenusMB($data, $parent_id = 0, $char = 'dl-submenu', $drop = 'dropdown-toggle', $togel = 'dropdown')
{
      $pa =[];
      foreach ($data as $key => $value) {
        array_push($pa,$value['parent']);
      }
      foreach ($data as $item)
      {
          if ($item['parent'] == $parent_id && in_array($item['id'],$pa) ) {
            if ($item['depth']>=1) {
              echo '<li   class="dropdown-submenu">';
            }else {
              echo '<li   class="">';
            }
            echo '<a href="' . $item['link'] . '" class="'.$drop.'" data-toggle="'.$togel.'">' . $item['label'] . '</a>';
            echo '<ul  class="'.$char.'">';
            showMenusMB($data, $item['id'], $char='dl-submenu', $drop='dropdown-toggle', $togel='dropdown');
            echo '</ul>';
            echo '</li>';
          }else if($item['parent'] == $parent_id) {
            echo '<li>';
            echo '<a href="' . $item['link'] . '">' . $item['label'] . '</a>';
            echo '</li>';
          }

      }

}


function categoryParent($datas ,$parent = 0, $str="", $select =0){
  foreach ($datas as $data) {
    $id = $data['id'];
    $title = $data['title'];
    if ($data['parent_id'] == $parent) {
      if ($select != 0 && $id == $select) {
        echo "<option value='$id' selected='selected' > $str $title </option>";
      }else {
        echo "<option value='$id'> $str $title </option>";
      }
      categoryParent($datas ,$id, $str."--", $select);
    }
  }

}



?>
