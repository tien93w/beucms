<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Slug;

class PostCategory extends Model
{
  protected $fillable = [
      'title', 'slug_id', 'description','parent_id'
  ];

  public function slugs()
  {
      return $this->hasOne('App\Slug','id','slug_id');
  }

  public function parent(){

    return $this->belongsTo('App\PostCategory','parent_id');

  }
}
