<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $fillable = [
      'title', 'slug_id', 'description','thumbnail','postcate_id','body'
  ];

  public function slugs()
  {
      return $this->hasOne('App\Slug','id','slug_id');
  }
}
