<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'slug_id', 'description','thumbnail','body'
    ];
  
    public function slugs()
    {
        return $this->hasOne('App\Slug','id','slug_id');
    }
}
