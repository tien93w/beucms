<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
      'title', 'slug_id', 'description','thumbnail','productcate_id','price','featured'
  ];

  public function slugs()
  {
      return $this->hasOne('App\Slug','id','slug_id');
  }
}
