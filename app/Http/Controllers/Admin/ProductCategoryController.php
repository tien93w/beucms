<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Slug;
use DataTables;
class ProductCategoryController extends Controller
{


  public function __construct()
  {
      $this->middleware('permission:cproduct_list', ['only' => ['index']]);
      $this->middleware('permission:cproduct_create', ['only' => ['create', 'store']]);
      $this->middleware('permission:cproduct_edit', ['only' => ['edit', 'update']]);
      $this->middleware('permission:cproduct_delete', ['only' => ['delete']]);
      $this->middleware('permission:cproduct_view', ['only' => ['show']]);

  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ProductCategory::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('parent', function($row){
    
                            $rolename = ""; 
                            if(!empty($row->parent)){
                                $rolename = $row->parent['title'];
                                
                                
                            }else
                            {
                                $rolename = "None";
                            }
    
                            return $rolename;
                    })
                    ->rawColumns(['parent'])
                    ->addColumn('action', function($row){
   
                        $btn = '<a  href="'.route('productcate.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-edit"></i>
                      </a> <a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                      <i class="la la-close"></i>
                    </a>';
  
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    
                    ->make(true);
        }
        return view('admin.productcate.index');
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {

      $datas = ProductCategory::all();
      return view('admin.productcate.create', compact('datas'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
        'title' => 'required',
        'slug' => 'required|unique:slugs',
        'description' => 'required',
    ]);
    $slug = new Slug;
    $slug->slug = $request->slug;
    $slug->type = 'productcate';
    $slug->save();
    $data = new ProductCategory;
    $data->title = $request->title;
    $data->parent_id = $request->parent_id;
    $data->slug_id = $slug->id;
    $data->description = $request->description;
    $data->save();
    return redirect()->route('productcate.index')
        ->with('success','Tạo mới danh mục thành công');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = ProductCategory::find($id);
    return view('admin.productcate.show',compact('data'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $datas = ProductCategory::all();
    $data = ProductCategory::find($id);
    return view('admin.productcate.edit',compact('data','datas'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $data = ProductCategory::find($id);
    $this->validate($request, [
        'title' => 'required',
        'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
        'description' => 'required',
    ]);
    
    $data->title = $request->title;
    $data->description = $request->description;
    $data->parent_id = $request->parent_id;
    $data->save();
    $slug = Slug::find($data->slug_id);
    $slug->slug = $request->slug;
    $slug->type = 'productcate';
    $slug->save();
    return redirect()->route('productcate.index')
        ->with('success','Sửa danh mục thành công');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {

    $data = ProductCategory::find($id);
    $data->delete();
    Slug::find($data->slug_id)->delete();

    return redirect()->route('productcate.index')
        ->with('success','Xóa danh mục thành công');
  }
}
