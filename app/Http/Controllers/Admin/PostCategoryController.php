<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\PostCategory;
use App\Slug;
class PostCategoryController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission:cpost_list', ['only' => ['index']]);
        $this->middleware('permission:cpost_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cpost_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cpost_delete', ['only' => ['delete']]);
        $this->middleware('permission:cpost_view', ['only' => ['show']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PostCategory::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('parent', function($row){
    
                            $rolename = ""; 
                            if(!empty($row->parent)){
                                $rolename = $row->parent['title'];
                                
                                
                            }else
                            {
                                $rolename = "None";
                            }
    
                            return $rolename;
                    })
                    ->rawColumns(['parent'])
                    ->addColumn('action', function($row){
   
                        $btn = '<a  href="'.route('postcate.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-edit"></i>
                      </a> <a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                      <i class="la la-close"></i>
                    </a>';
  
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    
                    ->make(true);
        }
        return view('admin.postcate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $datas = PostCategory::all();
        return view('admin.postcate.create',compact('datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'title' => 'required',
          'slug' => 'required|unique:slugs',
          'description' => 'required',
      ]);

      $slug = new Slug;
      $slug->slug = $request->slug;
      $slug->type = 'postcate';
      $slug->save();
      $data = new PostCategory;
      $data->title = $request->title;
      $data->parent_id = $request->parent_id;
      $data->slug_id = $slug->id;
      $data->description = $request->description;
      $data->save();
      return redirect()->route('postcate.index')
          ->with('success','Tọa mới danh mục thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = PostCategory::find($id);
      return view('admin.postcate.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $datas = PostCategory::all();
      $data = PostCategory::find($id);
      return view('admin.postcate.edit',compact('data','datas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PostCategory::find($id);
      $this->validate($request, [
          'title' => 'required',
          'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
          'description' => 'required',
      ]);

      
      $data->title = $request->title;
      $data->parent_id = $request->parent_id;
      $data->description = $request->description;
      $data->save();
      $slug = Slug::find($data->slug_id);
      $slug->slug = $request->slug;
      $slug->type = 'postcate';
      $slug->save();
      return redirect()->route('postcate.index')
          ->with('success','Sửa danh mục thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = PostCategory::find($id);
      $data->delete();
      Slug::find($data->slug_id)->delete();
      return redirect()->route('postcate.index')
          ->with('success','Xóa danh mục thành công');
    }
}
