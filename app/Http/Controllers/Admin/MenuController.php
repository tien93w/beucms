<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Facades\Menu;
use Harimayco\Menu\Models\MenuItems;
use App\Page;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:menu_list', ['only' => ['menulist']]);

    }
    public function menulist(){
      $menu = new Menus();
      $menuitems = new MenuItems();
      $menulist = $menu->select(['id', 'name'])->get();
      $menulist = $menulist->pluck('name', 'id')->prepend('Chọn Menu', 0)->all();
      $datas = Page::select()->orderBy('sort', 'asc')->get();

      if ((request()->has("action") && empty(request()->input("menu"))) || request()->input("menu") == '0') {
          return view('vendor.harimayco-menu.menu-html',compact($datas))->with("menulist", $menulist);
      } else {

          $menu = Menus::find(request()->input("menu"));
          $menus = $menuitems->getall(request()->input("menu"));

          $data = ['menus' => $menus, 'indmenu' => $menu, 'menulist' => $menulist];
          
          return view('vendor.harimayco-menu.menu-html', compact('data','datas'));
      }
    }

    public function menurender(){

      $datas = MenuItems::select()->orderBy('sort', 'asc')->get();
      return view('admin.menu.index',compact('datas'));
    }
}
