<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Response;
use Illuminate\Support\Facades\Storage;
class ImageController extends Controller
{
    function postImages(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file')) {
                $imageFiles = $request->file('file');
                // set destination path
                $folderDir = 'public/storage';
                $destinationPath = base_path() . '/' . $folderDir;
                // this form uploads multiple files
                foreach ($request->file('file') as $fileKey => $fileObject ) {
                    // make sure each file is valid
                    if ($fileObject->isValid()) {
                        // make destination file name
                        $destinationFileName = time() . $fileObject->getClientOriginalName();
                        // move the file from tmp to the destination path
                        $fileObject->move($destinationPath, $destinationFileName);
                        // save the the destination filename

                            return Response::json('success', 200);
                        
                    }
                }
            }
            

        }

}
function delImages(Request $request)
    {   //return response($request->name);
        // if ($request->ajax()) {
        //         // set destination path
        //         //$folderDir = 'storage/';
        //         $path = 'public/storage/'.$request->name;
        //         //$file = 'public/storage/'.$request->name;
        //         //$destinationPath = public_path() . '/' . $folderDir;
        //         //unlink($destinationPath.$request->name);
        //         File::delete($path);
        //         return 'Yup it worked!';
            
        // }
        $filename =  $request->get('name');
        $path=public_path().'/storage/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename; 
}

}
