<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slug;
use App\Page;
use DB;
use DataTables;
class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page_list', ['only' => ['index']]);
        $this->middleware('permission:page_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:page_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:page_delete', ['only' => ['delete']]);
        $this->middleware('permission:page_view', ['only' => ['show']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Page::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                        $btn = '<a  href="'.route('page.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-edit"></i>
                      </a> <a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                      <i class="la la-close"></i>
                    </a>';
  
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    
                    ->make(true);
        }
        return view('admin.page.index');
    }

    public function create()
    {
        return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'title' => 'required',
          'slug' => 'required|unique:slugs',
          'body'=>'required',
      ]);
      $slug = new Slug;
      $slug->slug = $request->slug;
      $slug->type = 'page';
      $slug->save();
      $data = new Page;
      $data->title = $request->title;
      $data->slug_id = $slug->id;
      $data->description = $request->description;
      $data->thumbnail = $request->thumbnail;
      $data->body = $request->body;
      $data->save();
      return redirect()->route('page.index')
          ->with('success','Tạo mới trang thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data2 = Page::find($id);
      return view('admin.page.edit',compact('data2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Page::find($id);
      $this->validate($request, [
          'title' => 'required',
          'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
          'body'=>'required',
      ]);

      
      $data->title = $request->title;
      $data->description = $request->description;
      $data->thumbnail = $request->thumbnail;
      $data->body = $request->body;
      $data->save();
      $slug = Slug::find($data->slug_id);
      $slug->slug = $request->slug;
      $slug->type = 'page';
      $slug->save();
      return redirect()->route('page.index')
          ->with('success','Sửa trang thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Page::find($id);
      $data->delete();
      Slug::find($data->slug_id)->delete();
      return redirect()->route('page.index')
          ->with('success','Xóa trang thành công');
    }
}

