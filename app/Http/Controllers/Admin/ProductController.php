<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use App\Slug;
use DataTables;
class ProductController extends Controller
{

  public function __construct()
  {
      $this->middleware('permission:product_list', ['only' => ['index']]);
      $this->middleware('permission:product_create', ['only' => ['create', 'store']]);
      $this->middleware('permission:product_edit', ['only' => ['edit', 'update']]);
      $this->middleware('permission:product_delete', ['only' => ['delete']]);
      $this->middleware('permission:product_view', ['only' => ['show']]);

  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                        $btn = '<a  href="'.route('product.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-edit"></i>
                      </a> <a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                      <i class="la la-close"></i>
                    </a>';
  
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    
                    ->make(true);
        }
        return view('admin.product.index');
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $data= ProductCategory::all();
      return view('admin.product.create',compact('data'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // $this->validate($request, [
    //     'title' => 'required',
    //     'slug' => 'required|unique:slugs',
    //     'description' => 'required',
    //     'thumbnail'=>'required',
    //     'price'=>'required',
    //     'body'=>'required',
    //     'productcate_id' => 'required'
    // ]);
    // $slug = new Slug;
    // $slug->slug = $request->slug;
    // $slug->type = 'product';
    // $slug->save();
    // $data = new Product;
    // $data->title = $request->title;
    // $data->featured = $request->featured;
    // $data->slug_id = $slug->id;
    // $data->description = $request->description;
    // $data->thumbnail = $request->thumbnail;
    // $data->body = $request->body;
    // $data->price = $request->price;
    // $data->productcate_id = $request->productcate_id;
    // $data->save();
    // return redirect()->route('product.index')
    //     ->with('success','Tạo mới sản phẩm thành công');

      if ($request->hasFile('file')) {
          $imageFiles = $request->file('file');
          // set destination path
          $folderDir = 'public/storage';
          $destinationPath = base_path() . '/' . $folderDir;
          // this form uploads multiple files
          foreach ($request->file('file') as $fileKey => $fileObject ) {
              // make sure each file is valid
              if ($fileObject->isValid()) {
                  // make destination file name
                  $destinationFileName = time() . $fileObject->getClientOriginalName();
                  // move the file from tmp to the destination path
                  $fileObject->move($destinationPath, $destinationFileName);
                  // save the the destination filename
                  
              }
          }
      
      

    }
    return response($request);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $data2 = Product::find($id);
    $data = ProductCategory::all();
    return view('admin.product.edit',compact('data2','data'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {$data = Product::find($id);
    $this->validate($request, [
        'title' => 'required',
        'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
        'description' => 'required',
        'thumbnail'=>'required',
        'price'=>'required',
        'body'=>'required',
        'productcate_id' => 'required'
    ]);
    
    $data->title = $request->title;
    $data->description = $request->description;
    $data->thumbnail = $request->thumbnail;
    $data->body = $request->body;
    $data->featured = $request->featured;
    $data->price = $request->price;
    $data->productcate_id = $request->productcate_id;
    $data->save();
    $slug = Slug::find($data->slug_id);
    $slug->slug = $request->slug;
    $slug->type = 'product';
    $slug->save();
    return redirect()->route('product.index')
        ->with('success','Sửa sản phẩm thành công');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $data = Product::find($id);
    $data->delete();
    Slug::find($data->slug_id)->delete();
    return redirect()->route('product.index')
        ->with('success','Xóa bài viết thành công');
  }
}
