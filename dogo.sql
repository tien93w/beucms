-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2019 at 02:50 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dogo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menus`
--

CREATE TABLE `admin_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menus`
--

INSERT INTO `admin_menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'Main', '2019-01-03 22:25:23', '2019-01-03 22:25:23'),
(5, 'Footer Menu', '2019-02-19 00:41:10', '2019-05-31 08:42:41'),
(6, 'Mobile Menu', '2019-05-31 08:42:11', '2019-05-31 08:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_items`
--

CREATE TABLE `admin_menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu_items`
--

INSERT INTO `admin_menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(2, 'Trang chủ', 'https://dogomynghethienminh.com/', 0, 0, NULL, 3, 0, '2019-01-03 22:45:55', '2019-01-09 10:06:53'),
(4, 'Sản phẩm', 'https://dogomynghethienminh.com/san-pham', 0, 1, NULL, 3, 0, '2019-01-03 23:49:37', '2019-01-09 10:06:48'),
(5, 'Giới thiệu', 'https://dogomynghethienminh.com/gioi-thieu', 0, 5, NULL, 3, 0, '2019-01-03 23:49:39', '2019-02-19 01:11:03'),
(6, 'Tin tức', 'https://dogomynghethienminh.com/tin-tuc', 0, 4, NULL, 3, 0, '2019-01-03 23:49:42', '2019-02-19 01:11:02'),
(7, 'Trang chủ', 'https://dogomynghethienminh.com/', 0, 0, NULL, 5, 0, '2019-02-19 00:41:23', '2019-05-31 08:42:41'),
(9, 'Nội thất phòng khách', 'noi-that-phong-khach', 4, 2, NULL, 3, 1, '2019-02-19 00:47:46', '2019-02-19 00:53:00'),
(10, 'Nội thất phòng ngủ', 'noi-that-phong-ngu', 0, 3, NULL, 3, 0, '2019-02-19 01:10:37', '2019-06-18 02:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `cart_contacts`
--

CREATE TABLE `cart_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_contacts`
--

INSERT INTO `cart_contacts` (`id`, `name`, `phone`, `email`, `content`, `message`, `created_at`, `updated_at`) VALUES
(3, 'Vũ Văn Tiến', '0916567747', 'tien2vv@gmail.com', '{\"012accea0909e79e42557f67260f9e31\":{\"rowId\":\"012accea0909e79e42557f67260f9e31\",\"id\":3,\"name\":\"Huawei MediaPad M2 Nh\\u1eadt (8 inch Dtab D-02h) Ch\\u1ea1y ROM qu\\u1ed1c t\\u1ebfsdasdasd\",\"qty\":\"1\",\"price\":2750000,\"options\":{\"image\":\"\\/photos\\/1\\/woraaaald.png\",\"slug\":\"huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-tesdasdasd\"},\"tax\":275000,\"subtotal\":2750000}}', NULL, '2019-01-07 03:50:50', '2019-01-07 03:50:50'),
(4, 'Vũ Văn Tiến', '0916567747', 'tien2vv@gmail.com', '{\"ea7963f77262c29a2eb582e06a656694\":{\"rowId\":\"ea7963f77262c29a2eb582e06a656694\",\"id\":5,\"name\":\"rterter rteert\",\"qty\":\"1\",\"price\":3890000,\"options\":{\"image\":\"\\/photos\\/1\\/IMG_20181209_140929.jpg\",\"slug\":\"rterter-rteert\"},\"tax\":389000,\"subtotal\":3890000},\"866e2ede7fc443781a2db1f9d0fdcf7c\":{\"rowId\":\"866e2ede7fc443781a2db1f9d0fdcf7c\",\"id\":4,\"name\":\"SONY XZ PREMIUM New Ch\\u1ea5t L\\u01b0\\u1ee3ng H\\u00e0ng \\u0110\\u1ea7u Vi\\u1ec7t Nam\",\"qty\":\"1\",\"price\":2290000,\"options\":{\"image\":\"\\/photos\\/1\\/woraaaald.png\",\"slug\":\"sony-xz-premium-new-chat-luong-hang-dau-viet-nam\"},\"tax\":229000,\"subtotal\":2290000},\"012accea0909e79e42557f67260f9e31\":{\"rowId\":\"012accea0909e79e42557f67260f9e31\",\"id\":3,\"name\":\"Huawei MediaPad M2 Nh\\u1eadt (8 inch Dtab D-02h) Ch\\u1ea1y ROM qu\\u1ed1c t\\u1ebfsdasdasd\",\"qty\":\"1\",\"price\":2750000,\"options\":{\"image\":\"\\/photos\\/1\\/woraaaald.png\",\"slug\":\"huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-tesdasdasd\"},\"tax\":275000,\"subtotal\":2750000}}', 'fyj gfhffftfhfh', '2019-01-07 04:00:07', '2019-01-07 04:00:07'),
(5, 'dasda', 'adasasd', 'adasdasdsad', '{\"bdfb6d9d8dbb377578c048a92ce6d535\":{\"rowId\":\"bdfb6d9d8dbb377578c048a92ce6d535\",\"id\":10,\"name\":\"T\\u01b0\\u1ee3ng Song M\\u00e3 g\\u1ed7 l\\u0169a b\\u00e1ch xanh 86x62x41cm\",\"qty\":\"2\",\"price\":0,\"options\":{\"image\":\"\\/photos\\/1\\/T\\u01b0\\u1ee3ng Song M\\u00e3 g\\u1ed7 l\\u0169a b\\u00e1ch xanh 86x62x41cm\\/do-go-my-nghe-thien-minh--tuong-song-ma-go-bach-xanh.jpg\",\"slug\":\"tuong-song-ma-go-lua-bach-xanh-86x62x41cm\"},\"tax\":0,\"subtotal\":0}}', 'asdasdsa', '2019-01-08 23:04:53', '2019-01-08 23:04:53'),
(6, 'sadasdas', 'adasdasd', 'adasasd', '[]', 'adasdas', '2019-01-08 23:05:39', '2019-01-08 23:05:39'),
(7, 'sdasdasd', 'adasdadad', 'asdasdas', '[]', 'dasdasdadsdas', '2019-01-08 23:06:06', '2019-01-08 23:06:06'),
(8, 'Vũ Văn Tiến', '916567747', 'tien2vv@gmail.com', '{\"eea9531580e8c4d624d4b6505520d9e8\":{\"rowId\":\"eea9531580e8c4d624d4b6505520d9e8\",\"id\":9,\"name\":\"B\\u1ea7u b\\u00ed nu h\\u01b0\\u01a1ng 40x40x15cm\",\"qty\":\"1\",\"price\":0,\"options\":{\"image\":\"\\/photos\\/1\\/B\\u1ea7u b\\u00ed nu h\\u01b0\\u01a1ng 40x40x15cm\\/do-go-my-nghe-thien-minh-bau-bi-lu-huong.jpg\",\"slug\":\"bau-bi-nu-huong-40x40x15cm\"},\"tax\":0,\"subtotal\":0}}', NULL, '2019-01-09 10:16:07', '2019-01-09 10:16:07'),
(9, 'Tạ Vũ Long', '981515838', 'kyo88kyo@gmail.com', '{\"a4394be8710264d53fc613d56444efb7\":{\"rowId\":\"a4394be8710264d53fc613d56444efb7\",\"id\":3,\"name\":\"Tranh t\\u1ee9 qu\\u00fd g\\u1ed7 h\\u01b0\\u01a1ng 88x42x3cm\",\"qty\":\"1\",\"price\":0,\"options\":{\"image\":\"\\/photos\\/1\\/Tranh t\\u1ee9 qu\\u00fd g\\u1ed7 h\\u01b0\\u01a1ng 88x42x3cm\\/do-go-my-nghe-thien-minh-tranh-tu-quy-go-huong.jpg\",\"slug\":\"tranh-tu-quy-go-huong-88x42x3cm\"},\"tax\":0,\"subtotal\":0}}', 'ádfasdf', '2019-01-25 16:54:04', '2019-01-25 16:54:04'),
(10, 'Vũ Văn Tiến', '916567747', 'tien2vv@gmail.com', '{\"eea9531580e8c4d624d4b6505520d9e8\":{\"rowId\":\"eea9531580e8c4d624d4b6505520d9e8\",\"id\":9,\"name\":\"B\\u1ea7u b\\u00ed nu h\\u01b0\\u01a1ng 40x40x15cm\",\"qty\":\"1\",\"price\":0,\"options\":{\"image\":\"\\/photos\\/1\\/B\\u1ea7u b\\u00ed nu h\\u01b0\\u01a1ng 40x40x15cm\\/do-go-my-nghe-thien-minh-bau-bi-lu-huong.jpg\",\"slug\":\"bau-bi-nu-huong-40x40x15cm\"},\"tax\":0,\"subtotal\":0}}', 'fhgfdc', '2019-01-25 16:55:02', '2019-01-25 16:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_21_144640_entrust_setup_tables', 1),
(9, '2018_01_03_064913_create_slugs_table', 7),
(10, '2018_12_24_121816_create_post_categories_table', 8),
(11, '2018_12_24_130613_create_product_categories_table', 8),
(12, '2018_12_24_133451_create_posts_table', 8),
(13, '2018_12_24_144419_create_products_table', 8),
(14, '2017_08_11_073824_create_menus_wp_table', 9),
(15, '2017_08_11_074006_create_menu_items_wp_table', 9),
(16, '2019_01_07_084451_create_shoppingcart_table', 10),
(17, '2019_01_07_104311_create_cart_contacts_table', 11),
(18, '2019_01_07_123032_create_pages_table', 12),
(19, '2019_06_04_150447_create_product_images_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_id` int(10) UNSIGNED NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug_id`, `thumbnail`, `description`, `body`, `created_at`, `updated_at`) VALUES
(2, 'Giới thiệu', 17, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', NULL, '<h1 class=\"title f-22 c-3e\">Ch&agrave;ng gi&aacute;m đốc l&agrave;ng b&aacute;n h&agrave;ng ra thế giới</h1>\r\n<p>&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong>Tốt nghiệp THPT với học lực loại giỏi nhưng ch&agrave;ng trai qu&ecirc; Từ Sơn- Bắc Ninh lại chọn học nghề, mở xưởng để lập nghiệp. Anh trở th&agrave;nh gi&aacute;m đốc th&agrave;nh đạt- &ldquo;vươn ra biển lớn&rdquo; cũng từ suy nghĩ v&agrave; &yacute; ch&iacute; kh&aacute;c người ấy.</strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong>Kh&aacute;c người</strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">Ở Ch&acirc;u Kh&ecirc;, Nguyễn Th&agrave;nh C&ocirc;ng được nhiều người biết đến với c&aacute;ch lập nghiệp kh&aacute; lạ l&ugrave;ng của anh. Tốt nghiệp THPT với học lực loại giỏi nhưng C&ocirc;ng kh&ocirc;ng theo con đường học h&agrave;nh thi cử, anh lập nghiệp bằng việc học nghề, mở xưởng. Ch&acirc;u Kh&ecirc; ng&agrave;y ấy c&oacute; nghề truyền thống sắt th&eacute;p rất ph&aacute;t triển, với h&agrave;ng ngh&igrave;n hộ tham gia sản xuất. Thế nhưng C&ocirc;ng kh&ocirc;ng chọn nghề sản xuất sắt th&eacute;p, anh theo học nghề gỗ mỹ nghệ.</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">Quyết định đ&oacute; khiến những người trong gia đ&igrave;nh ngạc nhi&ecirc;n. &ldquo;Mọi người n&oacute;i t&ocirc;i lạ l&ugrave;ng, nhưng t&ocirc;i thấy nghề gỗ mỹ nghệ l&uacute;c đ&oacute; rất c&oacute; tương lai. Thu nhập của người d&acirc;n ng&agrave;y một cao, l&uacute;c đ&oacute; người ta sẽ quan t&acirc;m nhiều đến việc ở thế n&agrave;o. Nh&agrave; sẽ được x&acirc;y to hơn, đẹp hơn v&agrave; đương nhi&ecirc;n kh&ocirc;ng thể thiếu nội thất. Đ&oacute; ch&iacute;nh l&agrave; thời của đồ gỗ. V&agrave; l&uacute;c đ&oacute; t&ocirc;i thật may mắn, mọi người trong gia đ&igrave;nh đều ủng hộ&rdquo;, C&ocirc;ng chia sẻ.</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><img src=\"/photos/1/20110425145235_f34vancong0201111.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">Nghĩ l&agrave; l&agrave;m, C&ocirc;ng khăn g&oacute;i khắp c&aacute;c nơi sản xuất đồ gỗ mỹ nghệ để học nghề, hỏi kinh nghiệm. Sản phẩm anh quan t&acirc;m học hỏi nhất l&uacute;c đ&oacute; l&agrave; sập gụ, tủ ch&egrave;, b&agrave;n thờ mỹ nghệ, tủ rượu, giường ngủ mỹ nghệ&hellip; Sau thời gian học việc, t&iacute;ch luỹ kinh nghiệm, x&acirc;y dựng c&aacute;c mối quan hệ l&agrave;m ăn, C&ocirc;ng về qu&ecirc; mở xưởng.&nbsp;<br /><br />Đ&acirc;y cũng l&agrave; thời kỳ kh&oacute; khăn nhất của C&ocirc;ng. Đầu ti&ecirc;n l&agrave; thiếu vốn v&agrave; chưa c&oacute; kinh nghiệm quản l&yacute;. To&agrave;n bộ số vốn ban đầu khi mở xưởng anh phải vay mượn từ người th&acirc;n, bạn b&egrave;. Gian nan hơn cả l&agrave; việc t&igrave;m đầu ra v&igrave; thị trường đồ gỗ mỹ nghệ l&uacute;c đ&oacute; hầu như đ&atilde; bị c&aacute;c l&agrave;ng đồ gỗ mỹ nghệ nổi tiếng chiếm lĩnh.&nbsp;<br /><br />Kh&ocirc;ng nản l&ograve;ng, C&ocirc;ng t&igrave;m được giải ph&aacute;p gia c&ocirc;ng cho c&aacute;c cơ sở sản xuất đồ gỗ mỹ nghệ kh&aacute;c m&agrave; anh quen biết khi học việc. Chỉ một thời gian ngắn, xưởng sản xuất của C&ocirc;ng đ&atilde; dần c&oacute; chỗ đứng, sản xuất dần đi v&agrave;o ổn định tuy chỉ l&agrave; một cơ sở sản xuất nhỏ.&nbsp;<br /><br />&ldquo;Nếu kh&ocirc;ng mở C&ocirc;ng ty th&igrave; kh&ocirc;ng thể sản xuất lớn được, sản phẩm của m&igrave;nh cũng sẽ m&atilde;i m&atilde;i kh&ocirc;ng c&oacute; t&ecirc;n, chỉ c&oacute; thể đi l&agrave;m gia c&ocirc;ng cho những doanh nghiệp kh&aacute;c&rdquo;, C&ocirc;ng n&oacute;i. Tuy nhi&ecirc;n, mở C&ocirc;ng ty l&agrave; vấn đề lớn. Để thực hiện kế hoạch n&agrave;y, C&ocirc;ng đăng k&yacute; học kh&oacute;a quản trị kinh doanh. Đến năm 2007, C&ocirc;ng ty CP XNK Thi&ecirc;n Minh ra đời. Từ năm đ&oacute;, thương hiệu gỗ mỹ nghệ Thi&ecirc;n Minh đ&atilde; dần được nhiều kh&aacute;ch h&agrave;ng biết đến.<br /><br /><strong>Vươn ra biển lớn</strong><br /><br />C&ocirc;ng x&uacute;c tiến đăng k&yacute; sở hữu tr&iacute; tuệ c&aacute;c sản phẩm do C&ocirc;ng ty của m&igrave;nh thiết kế, sản xuất. To&agrave;n bộ c&aacute;c sản phẩm mang thương hiệu Đồ gỗ mỹ nghệ Thi&ecirc;n Minh. Sau đ&oacute;, gi&aacute;m đốc l&agrave;ng bắt tay thực hiện lu&ocirc;n việc quảng b&aacute; thương hiệu Thi&ecirc;n Minh&nbsp;tr&ecirc;n Internet.&nbsp;<br /><br />Từ doanh thu 5 tỷ đồng năm 2007, đến hết năm 2009 doanh thu của C&ocirc;ng ty đ&atilde; l&ecirc;n hơn 30 tỷ đồng, năm 2010 l&agrave; hơn 100 tỷ đồng. H&agrave;ng năm doanh nghiệp của C&ocirc;ng đ&atilde; giải quyết việc l&agrave;m cho h&agrave;ng trăm lao động địa phương với mức thu nhập 5 - 7 triệu đồng/người.<br /><br />Năm 2015, trang web giới thiệu về c&aacute;c sản phẩm của c&ocirc;ng ty đ&atilde; ra đời. Tại đ&acirc;y, to&agrave;n bộ c&aacute;c sản phẩm được trưng b&agrave;y bằng h&igrave;nh ảnh chi tiết, gi&aacute; cả cụ thể cũng như việc thanh to&aacute;n, vận chuyển, đặc biệt l&agrave; c&aacute;c dịch vụ k&egrave;m theo như tư vấn b&aacute;n h&agrave;ng, tư vấn thiết kế nội thất.&nbsp;<br /><br />Cũng thời gian n&agrave;y, ngo&agrave;i những mặt h&agrave;ng gỗ mỹ nghệ theo mẫu m&atilde; truyền thống, C&ocirc;ng sản xuất th&ecirc;m mặt h&agrave;ng đồ gỗ nội thất hiện đại như ghế sofa, bộ tủ bếp, đồ gỗ ph&ograve;ng kh&aacute;ch, văn ph&ograve;ng, đồ gỗ ph&ograve;ng karaoke&hellip;&nbsp;<br /><br />Đồng thời C&ocirc;ng cũng trực tiếp mang sản phẩm của C&ocirc;ng ty m&igrave;nh giới thiệu tại c&aacute;c hội chợ quốc tế tổ chức ở thị trường ngo&agrave;i nước như Hồng K&ocirc;ng, Trung Quốc v&agrave; Asean... Năm 2010, C&ocirc;ng x&acirc;y dựng gian trưng b&agrave;y sản phẩm hơn 6.000 m2 tại M&oacute;ng C&aacute;i (Quảng Ninh) nhằm quảng b&aacute; v&agrave; xuất h&agrave;ng sang Trung Quốc.</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<h2 style=\"font-size: 20px; line-height: 21px;\"><strong>Ph&ograve;ng l&agrave;m việc của Tổng gi&aacute;m đốc</strong></h2>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong><img src=\"/photos/1/IMG_20181209_141114.jpg\" alt=\"\" width=\"100%\" /></strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong><img src=\"/photos/1/IMG_20181209_140929.jpg\" alt=\"\" width=\"100%\" /></strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong><img src=\"/photos/1/IMG_20181209_141051.jpg\" alt=\"\" width=\"100%\" /></strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<h2 style=\"font-size: 20px; line-height: 21px;\"><strong>Xưởng sản xuất</strong></h2>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><strong><img src=\"/photos/1/IMG_20181209_142207.jpg\" alt=\"\" width=\"100%\" /></strong></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><img src=\"/photos/1/IMG_20181209_142311.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><img src=\"/photos/1/IMG_20181209_141717.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px;\"><img src=\"/photos/1/IMG_20181209_141956.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p style=\"font-size: 14px; line-height: 21px;\">&nbsp;</p>\r\n<p style=\"font-size: 14px; line-height: 21px; margin-bottom: 30px;\"><img src=\"/photos/1/IMG_20181209_141909.jpg\" alt=\"\" width=\"100%\" /></p>', '2019-01-07 05:45:02', '2019-05-31 09:39:09'),
(3, 'Liên hệ', 18, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', NULL, '<p>đ&acirc;y l&agrave; trang li&ecirc;n hệ</p>', '2019-01-07 05:45:29', '2019-05-31 09:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'role_edit', 'role edit', 'role edit', NULL, NULL),
(2, 'role_list', 'Role List', 'Role List', NULL, NULL),
(3, 'role_delete', 'Role Delete', 'Role Delete', NULL, NULL),
(4, 'role_create', 'role_create', 'role_create', NULL, NULL),
(5, 'role_view', 'role_view', 'role_view', NULL, NULL),
(6, 'user_list', 'user_list', 'user_list', NULL, NULL),
(7, 'user_edit', 'user_edit', 'user_edit', NULL, NULL),
(8, 'user_delete', 'user_delete', 'user_delete', NULL, NULL),
(9, 'user_create', 'user_create', 'user_create', NULL, NULL),
(10, 'user_view', 'user_view', 'user_view', NULL, NULL),
(11, 'menu_list', 'Menu', 'Menu', NULL, NULL),
(12, 'page_list', 'Danh sách trang', 'Danh sách trang', NULL, NULL),
(13, 'page_create', 'Tạo mới trang', 'Tạo mới trang', NULL, NULL),
(14, 'page_edit', 'Sửa trang', 'Sửa trang', NULL, NULL),
(15, 'page_delete', 'Xóa trang', 'Xóa trang', NULL, NULL),
(16, 'page_view', 'Xem trang', 'Xem trang', NULL, NULL),
(17, 'cpost_list', 'danh sách danh mục bài viết', 'danh sách danh mục bài viết', NULL, NULL),
(18, 'cpost_create', 'Tạo mới danh mục bài viết', 'Tạo mới danh mục bài viết', NULL, NULL),
(19, 'cpost_edit', 'Sửa danh mục bài viết', 'Sửa danh mục bài viết', NULL, NULL),
(20, 'cpost_delete', 'Xóa danh mục bài viết', 'Xóa danh mục bài viết', NULL, NULL),
(21, 'cpost_view', 'Xem danh mục bài viết', 'Xem danh mục bài viết', NULL, NULL),
(22, 'post_list', 'Danh sách bài viết', 'Danh sách bài viết', NULL, NULL),
(23, 'post_create', 'Tạo mới bài viết', 'Tạo mới bài viết', NULL, NULL),
(24, 'post_edit', 'Sửa bài viết', 'Sửa bài viết', NULL, NULL),
(25, 'post_delete', 'Xóa bài viết', 'Xóa bài viết', NULL, NULL),
(26, 'post_view', 'Xem bài viết', 'Xem bài viết', NULL, NULL),
(27, 'cproduct_list', 'Danh sách danh mục sản phẩm', 'Danh sách danh mục sản phẩm', NULL, NULL),
(28, 'cproduct_create', 'Tạo mới danh mục sản phẩm', 'Tạo mới danh mục sản phẩm', NULL, NULL),
(29, 'cproduct_edit', 'Sửa danh mục sản phẩm', 'Sửa danh mục sản phẩm', NULL, NULL),
(30, 'cproduct_delete', 'Xóa danh mục sản phẩm', 'Xóa danh mục sản phẩm', NULL, NULL),
(31, 'cproduct_view', 'Xem danh mục sản phẩm', 'Xem danh mục sản phẩm', NULL, NULL),
(32, 'product_list', 'Danh sách sản phẩm', 'Danh sách sản phẩm', NULL, NULL),
(33, 'product_create', 'Tạo mới sản phầm', 'Tạo mới sản phầm', NULL, NULL),
(34, 'product_edit', 'Sửa sản phẩm', 'Sửa sản phẩm', NULL, NULL),
(35, 'product_delete', 'Xóa sản phẩm', 'Xóa sản phẩm', NULL, NULL),
(36, 'product_view', 'Xem sản phẩm', 'Xem sản phẩm', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_id` int(10) UNSIGNED NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcate_id` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug_id`, `thumbnail`, `description`, `body`, `postcate_id`, `created_at`, `updated_at`) VALUES
(2, 'Vì sao cây gỗ trả giá 1 triệu USD không bán ở Thanh Hóa lại được xem là \"báu vật\"?', 8, '/photos/1/sasassa.jpg', '<p>cghhchg</p>', '<p>&nbsp;</p>\r\n<p><strong>Nhiều đại gia săn l&ugrave;ng v&agrave; sẵn s&agrave;ng trả gi&aacute; \"tr&ecirc;n trời\" để được sở hữu gỗ nu, v&igrave; đ&acirc;y l&agrave; một trong những \"b&aacute;u vật\" đắt gi&aacute; nhất của c&aacute;c lo&agrave;i c&acirc;y gỗ.</strong></p>\r\n<p>Sự việc c&acirc;y&nbsp;gỗ nu hương đỏ ở Thanh H&oacute;a l&agrave; một v&iacute; dụ điển h&igrave;nh. H&agrave;ng trăm đại gia sẵn s&agrave;ng trả gi&aacute; cao để mua c&acirc;y gỗ qu&yacute; vốn c&oacute; vẻ ngo&agrave;i kh&ocirc; cằn kh&ocirc;ng hề nổi bật. Thậm ch&iacute;, c&oacute; người c&ograve;n trả gi&aacute; 1 triệu USD để sở hữu \"b&aacute;u vật\" nhưng vẫn bị từ chối.</p>\r\n<p>Trước đ&oacute;, theo&nbsp;<em>D&acirc;n Tr&iacute;</em>, một bộ b&agrave;n ghế được chế t&aacute;c từ gốc c&acirc;y gỗ nu rừng của một nghệ nh&acirc;n người Th&aacute;i B&igrave;nh từng được rao b&aacute;n với gi&aacute; tr&ecirc;n 1 tỷ đồng v&agrave; đ&atilde; g&acirc;y sốt tại Trung t&acirc;m Triển l&atilde;m v&agrave; Hội chợ h&agrave;ng thủ c&ocirc;ng mỹ nghệ Việt Nam ở Ho&agrave;ng Th&agrave;nh Thăng Long, H&agrave; Nội v&agrave;o th&aacute;ng 8/2016.</p>\r\n<p><img src=\"/photos/1/do-go-my-nghe-thien-minh-tin-tuc.png\" alt=\"\" width=\"100%\" /></p>\r\n<div class=\"VCSortableInPreviewMode active\">\r\n<div class=\"PhotoCMS_Caption\">\r\n<p data-placeholder=\"[nhập ch&uacute; th&iacute;ch]\">C&oacute; người trả gi&aacute; 1 triệu USD để được sở hữu c&acirc;y gỗ nu hương đỏ ở Thanh H&oacute;a. Ảnh: Internet</p>\r\n</div>\r\n</div>\r\n<p>Mới chừng đ&oacute; th&ocirc;i, ch&uacute;ng ta c&oacute; thể thấy gi&aacute; trị kh&ocirc;ng hề nhỏ của gỗ nu trong c&aacute;c loại gỗ qu&yacute;, đặc biệt l&agrave; n&oacute; c&oacute; sức h&uacute;t rất lớn đối với những đại gia chuy&ecirc;n săn t&igrave;m \"m&oacute;n qu&agrave;\" qu&yacute; gi&aacute; của tự nhi&ecirc;n n&agrave;y.</p>\r\n<p><strong>Tại sao gỗ nu lại đắt như vậy?</strong></p>\r\n<p>Theo một trang web uy t&iacute;n về sản phẩm gỗ&nbsp;chia sẻ, nu thực chất kh&ocirc;ng phải l&agrave; một loại c&acirc;y lấy gỗ giống như c&aacute;c loại hương, sưa, đỏ, ho&agrave;ng đ&agrave;n, gụ, gi&aacute;ng hương,... m&agrave; thực chất n&oacute; chỉ l&agrave; một phần \"dị tật xấu x&iacute;\" nh&ocirc; ra từ c&aacute;c th&acirc;n c&acirc;y gỗ.</p>\r\n<p>Gỗ nu l&agrave; t&ecirc;n gọi chung của c&aacute;c c&acirc;y gỗ qu&yacute; hiếm m&agrave; c&oacute; phần \"dị tật\" nh&ocirc; ra ở th&acirc;n c&acirc;y. Nu c&oacute; thể được sinh ra từ c&aacute;c vết dị tật, vết thương tr&ecirc;n những th&acirc;n c&acirc;y c&oacute; tuổi thọ lớn do bị chặt ch&eacute;m, s&eacute;t đ&aacute;nh hoặc đơn giản l&agrave; do bị g&atilde;y tr&ecirc;n c&aacute;c th&acirc;n c&acirc;y c&oacute; tuổi thọ lớn v&agrave; qu&yacute; hiếm.</p>\r\n<p><img src=\"/photos/1/photo-1-1508231846519.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<div class=\"VCSortableInPreviewMode\">\r\n<div class=\"PhotoCMS_Caption\">\r\n<p data-placeholder=\"[nhập ch&uacute; th&iacute;ch]\">Gỗ nu thường c&oacute; v&acirc;n gỗ kỳ dị rất hiếm gặp. Ảnh: Internet</p>\r\n</div>\r\n<div>&nbsp;</div>\r\n<div id=\"abde\">&nbsp;</div>\r\n</div>\r\n<p>Th&ocirc;ng thường, nu tr&ecirc;n c&aacute;c th&acirc;n c&acirc;y kh&ocirc;ng nhiều v&agrave; ch&uacute;ng ph&aacute;t triển kh&aacute; t&aacute;ch biệt với c&aacute;c phần c&ograve;n lại của c&acirc;y với h&igrave;nh dạng \"kỳ dị\", cứng nhưng c&oacute; nhiều v&acirc;n gỗ rất đẹp. Qua thời gian, những phần nh&ocirc; l&ecirc;n ng&agrave;y n&agrave;o dần t&iacute;ch tụ v&agrave; trở n&ecirc;n sần s&ugrave;i tr&ecirc;n th&acirc;n c&acirc;y qua h&agrave;ng chục, h&agrave;ng trăm năm v&agrave; tạo th&agrave;nh nu gỗ qu&yacute; hiếm.&nbsp;</p>\r\n<p>Gỗ nu thường c&oacute; gi&aacute; trị rất cao v&agrave; mang lại vẻ đẹp rất ri&ecirc;ng biệt cho những sản phẩm đồ gỗ mỹ nghệ đắt tiền.</p>\r\n<p>Trong số c&aacute;c loại gỗ nu như&nbsp;<em>nu hương, nu nghiến, nu sưa</em>,... nhưng phổ biến nhất vẫn l&agrave; nu nghiến.</p>\r\n<p>Gỗ nu c&oacute; v&acirc;n gỗ đẹp kỳ lạ, hầu như kh&ocirc;ng theo bất kỳ một quy luật n&agrave;o. Do đ&oacute;, những người thợ mộc mới v&agrave;o nghề hoặc non kinh nghiệm hay thậm ch&iacute; l&agrave; cao tay thường kh&ocirc;ng d&aacute;m nhận chế t&aacute;c loại gỗ n&agrave;y v&igrave; mất rất nhiều thời gian v&agrave; nếu kh&ocirc;ng kh&eacute;o th&igrave; gỗ nu rất dễ nứt vỡ v&agrave; xuống m&agrave;u.</p>\r\n<p>do c&aacute;ch h&igrave;nh th&agrave;nh đặc biệt n&ecirc;n v&acirc;n gỗ nu kh&ocirc;ng theo một sự sắp xếp cố định, v&acirc;n xoắn ngẫu nhi&ecirc;n tạo những h&igrave;nh th&ugrave; vền vện, kh&ocirc;ng c&oacute; tim gỗ do đ&oacute; rất kh&oacute; chế t&aacute;c.&nbsp;</p>\r\n<p>Một nghệ nh&acirc;n chia sẻ với D&acirc;n Tr&iacute;, do c&aacute;ch h&igrave;nh th&agrave;nh đặc biệt n&ecirc;n v&acirc;n của gỗ nu thường kh&ocirc;ng theo một sự sắp xếp cố định, v&acirc;n xoắn ngẫu nhi&ecirc;n tạo n&ecirc;n những h&igrave;nh th&ugrave; vền vện, kh&ocirc;ng c&oacute; tim gỗ, do đ&oacute; rất kh&oacute; chế t&aacute;c. Để tạo n&ecirc;n những đồ vật chế t&aacute;c từ gỗ nu tinh xảo thậm ch&iacute; c&oacute; thể mất tới h&agrave;ng năm trời.</p>\r\n<p>Hơn nữa, sỡ dĩ gỗ nu qu&yacute; hiếm v&agrave; được coi như l&agrave; \"b&aacute;u vật\" trong c&aacute;c loại gỗ v&igrave;&nbsp;<em>kh&ocirc;ng phải cứ th&acirc;n c&acirc;y lớn l&agrave; sẽ c&oacute; nu</em>. Chẳng hạn, loại gỗ qu&yacute; như sưa m&agrave; cả trăm c&acirc;y mới bắt gặp được một đến hai c&acirc;y c&oacute; nu.</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, nhiều người cho rằng gỗ nu c&oacute; thể mang lại may mắn, t&agrave;i lộc v&agrave; thịnh vượng cho gia chủ. Do đ&oacute;, kh&ocirc;ng &iacute;t người sẵn s&agrave;ng d&agrave;nh nhiều thời gian săn t&igrave;m v&agrave; bỏ ra một số tiền lớn để sở hữu loại gỗ qu&yacute; gi&aacute; n&agrave;y.</p>\r\n<div id=\"admzone480455\">Theo đ&oacute;, những sản phẩm mỹ nghệ l&agrave;m từ gỗ nu cũng c&oacute; gi&aacute; trị rất lớn v&agrave; được nhiều đại gia mua v&agrave; b&agrave;y tr&iacute; cho ng&ocirc;i nh&agrave; của m&igrave;nh.&nbsp;</div>\r\n<p>Chỉ với chừng ấy l&yacute; do cũng c&oacute; thể thấy được gi&aacute; trị v&agrave; vị tr&iacute; của gỗ nu trong c&aacute;c loại gỗ qu&yacute;. N&oacute; thực sự l&agrave; một b&aacute;u vật m&agrave; thi&ecirc;n nhi&ecirc;n tạo ra.</p>\r\n<p style=\"text-align: right;\"><em>Nguồn: soha/Tr&iacute; thức trẻ</em></p>', 1, '2019-01-06 22:26:09', '2019-01-08 19:31:56'),
(3, 'Đại gia “săn” đồ gỗ mỹ nghệ vài trăm triệu ở chợ hoa xuân TP.HCM', 12, '/photos/1/1485155763-5.jpg', '<p>sdfgsdg</p>', '<p>&nbsp;</p>\r\n<p><strong>Ngo&agrave;i h&agrave;ng ng&agrave;n hoa kiểng đang được b&agrave;y b&aacute;n ở chợ hoa xu&acirc;n Ph&uacute; Mỹ Hưng, Q.7, TP.HCM, c&oacute; kh&ocirc;ng &iacute;t đồ gỗ mỹ nghệ chế t&aacute;c tinh xảo với gi&aacute; b&aacute;n v&agrave;i trăm triệu đồng mỗi m&oacute;n được c&aacute;c đại gia t&igrave;m mua&hellip;</strong></p>\r\n<p>Tại chợ hoa xu&acirc;n khu đ&ocirc; thị Ph&uacute; Mỹ Hưng năm nay, ngo&agrave;i phần lớn diện t&iacute;ch được ban tổ chức bố tr&iacute; cho c&aacute;c nh&agrave; vườn b&agrave;y b&aacute;n hoa kiểng th&igrave; một số gian h&agrave;ng b&aacute;n đồ gỗ mỹ nghệ cũng xuất hiện. Tuy số lượng kh&aacute; &iacute;t nhưng c&aacute;c loại đồ gỗ mỹ nghệ được c&aacute;c nghệ nh&acirc;n b&agrave;y b&aacute;n tại chợ hoa lại c&oacute; gi&aacute; trị rất cao, từ v&agrave;i chục đến v&agrave;i trăm triệu đồng mỗi m&oacute;n.</p>\r\n<p>Anh Nhẫn, chủ cơ sở đồ gỗ Mai Nhẫn cho biết, những ng&agrave;y qua cửa h&agrave;ng anh b&aacute;n kh&aacute; chạy, doanh thu l&ecirc;n đến cả tỷ đồng. Ngo&agrave;i những bộ b&agrave;n ghế c&oacute; gi&aacute; b&aacute;n gần 200 triệu đồng, gian h&agrave;ng của anh c&ograve;n c&oacute; nhiều loại ph&ugrave; đi&ecirc;u chạm nổi với gi&aacute; b&aacute;n 50 triệu đồng.</p>\r\n<p><img src=\"/photos/1/do-go-my-nghe-thien-minh.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>Đ&aacute;p ứng nhu cầu người mua, năm nay cơ sở đồ gỗ Mai Nhẫn tung ra thị trường những bức ph&ugrave; đi&ecirc;u k&iacute;ch thước lớn trong đ&oacute; c&aacute;c tượng nhỏ b&ecirc;n trong được đ&uacute;c từ nguy&ecirc;n liệu bột gỗ v&agrave; composite. Những bức ph&ugrave; đi&ecirc;u n&agrave;y được b&aacute;n với gi&aacute; 14 triệu đồng.</p>\r\n<p><img src=\"/photos/1/1485155763-9.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/1485155763-10.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p style=\"text-align: right;\"><em>Nguồn: 24h.com.vn</em></p>', 1, '2019-01-07 02:09:53', '2019-01-08 19:32:46'),
(4, 'Đồ gỗ tiền tỉ xuống phố, người mua trầm trồ', 13, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', '<p>sdfdsf</p>', '<p>&nbsp;</p>\r\n<p><strong>B&ecirc;n cạnh sự phong ph&uacute; về c&aacute;c loại hoa, hội chợ xu&acirc;n Đ&agrave; Nẵng năm nay xuất hiện những bộ phản gỗ, b&agrave;n ghế gỗ tiền tỉ khiến người mua kh&ocirc;ng khỏi trầm trồ.</strong></p>\r\n<p>Khu&ocirc;n vi&ecirc;n ph&iacute;a Bắc tượng đ&agrave;i 2 Th&aacute;ng 9 (quận Hải Ch&acirc;u, Đ&agrave; Nẵng) năm nay tấp nập kẻ b&aacute;n, người mua với gần chục ki-ốt b&aacute;n&nbsp;<a title=\"đồ gỗ\" href=\"http://soha.vn/do-go.html\" target=\"_blank\" rel=\"noopener\">đồ gỗ</a>&nbsp;mỹ nghệ cao cấp.</p>\r\n<p>Ngay cổng v&agrave;o khu hội chợ n&agrave;y, một doanh nghiệp từ huyện Thăng B&igrave;nh (Quảng Nam) mang ra tr&igrave;nh l&agrave;ng những bộ phản gỗ qu&yacute; hiếm. Tuổi đời gỗ từ v&agrave;i chục năm đến trăm năm.</p>\r\n<p>Kh&aacute;ch tham quan c&ugrave;ng người mua kh&oacute; rời mắt trước tấm phản gỗ hương d&agrave;i đến 7m, rộng 1,5m, d&agrave;y 18cm. Chủ nh&acirc;n tấm phản k&ecirc; gi&aacute; 600 triệu đồng.</p>\r\n<p><img src=\"/photos/1/photo-1-15186021643101833209282.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>Cạnh đ&oacute;, tấm phản gỗ cẩm lai d&agrave;i 4m, rộng 1,8m, d&agrave;y 20cm c&oacute; gi&aacute; đến 1,15 tỉ đồng. Ngay ch&iacute;nh giữa gian h&agrave;ng n&agrave;y l&agrave; tấm phản đắt nhất với gi&aacute; 2,25 tỉ đồng. Đ&acirc;y l&agrave; phản gỗ cẩm lai d&agrave;i 4m, rộng 2,1m, d&agrave;y 35cm.</p>\r\n<p>Một tấm phản gỗ cẩm lai b&ecirc;n cạnh d&agrave;i 4,8m, rộng 2m, d&agrave;y 20cm cũng được h&eacute;t gi&aacute; đến 1,25 tỉ đồng.</p>\r\n<p>&Ocirc;ng Ch&acirc;u (người d&acirc;n Đ&agrave; Nẵng) trầm trồ: &ldquo;Cả đời t&ocirc;i chưa thấy nhiều gỗ qu&yacute; hiếm m&agrave; đắt gi&aacute; đến vậy. Chắc chỉ c&oacute; h&agrave;ng đại gia mới d&aacute;m sờ tay đến&rdquo;.</p>\r\n<p>Ngo&agrave;i ra, gần chục gian h&agrave;ng c&ograve;n trưng b&agrave;y đủ loại b&agrave;n ghế, tượng gỗ&hellip; được chạm khắc cực kỳ tinh xảo. Mỗi sản phẩm c&oacute; gi&aacute; từ 5 triệu đến h&agrave;ng chục triệu đồng.</p>\r\n<p>Theo chủ một gian h&agrave;ng, những sản phẩm gỗ đắt gi&aacute; được đưa xuống hội chợ lần n&agrave;y rất k&eacute;n kh&aacute;ch v&igrave; gi&aacute; khủng. Mục đ&iacute;ch ch&iacute;nh của họ l&agrave; quảng b&aacute; thương hiệu. Hội chợ c&ograve;n k&eacute;o d&agrave;i đến hết h&ocirc;m nay (14-2, tức 29 Tết).</p>\r\n<p style=\"text-align: right;\"><em>Nguồn: soha.vn</em></p>', 1, '2019-01-07 02:12:15', '2019-05-31 10:57:08'),
(5, 'Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi', 14, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', '<p>dsfsdf sdfdsfdsf fsd</p>', '<p style=\"margin-top: 20px;\"><strong>Tại chợ xu&acirc;n TP. Bu&ocirc;n Ma Thuột (Đắk Lắk) năm 2018, b&ecirc;n cạnh những mặt h&agrave;ng như mai, đ&agrave;o, c&uacute;c, quất cảnh&hellip; c&ograve;n c&oacute; sự hiện diện của rất nhiều quầy h&agrave;ng đồ gỗ mỹ nghệ với gi&aacute; từ v&agrave;i triệu đồng đến h&agrave;ng trăm triệu đồng mỗi sản phẩm.</strong></p>\r\n<p>Từ nhiều năm nay, cứ mỗi độ tết đến người d&acirc;n tại phố n&uacute;i Bu&ocirc;n Ma Thuột lại c&oacute; nhu cầu mua c&aacute;c loại đồ gỗ mỹ nghệ về chưng tết. Ch&iacute;nh v&igrave; vậy, mỗi năm số lượng những mặt h&agrave;ng n&agrave;y lại c&agrave;ng đa dạng, phong ph&uacute; từ mẫu m&atilde; đến chủng loại để phục vụ kh&aacute;ch h&agrave;ng.</p>\r\n<p><img src=\"/photos/1/c10-15179725083532049766442.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>Tại cửa h&agrave;ng đồ gỗ mỹ nghệ Lộc Ph&aacute;t, anh Ki&ecirc;n (chủ cửa h&agrave;ng) khoe với ch&uacute;ng t&ocirc;i đ&atilde; b&aacute;n được một số sản phẩm khi vừa mới trưng b&agrave;y gian h&agrave;ng. Anh Ki&ecirc;n cho biết, mặt h&agrave;ng người d&acirc;n ưa chuộng hiện nay đ&oacute; l&agrave; tượng phật Di Lặc, tượng Tam đa, lục b&igrave;nh&hellip;</p>\r\n<p>Về gi&aacute; cả của những bức tượng n&agrave;y, anh Ki&ecirc;n chia sẻ mỗi sản phẩm c&oacute; gi&aacute; từ v&agrave;i triệu đến h&agrave;ng chục triệu đồng t&ugrave;y v&agrave;o k&iacute;ch thước cũng như độ qu&yacute; hiếm của c&aacute;c loại gỗ. Sản phẩm c&oacute; gi&aacute; cao nhất trong gian h&agrave;ng của anh l&agrave; bức tượng rồng thi&ecirc;ng l&agrave;m bằng gỗ dổi đ&aacute; được rao b&aacute;n gi&aacute; 250 triệu đồng v&agrave; c&aacute;c sản phẩm c&ograve;n lại dao động từ 5 &ndash; 50 triệu đồng.</p>\r\n<p><img src=\"/photos/1/c6-1517972508345506740360.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>Theo anh Nguyễn Xu&acirc;n Ho&agrave;ng (ngụ TP. Bu&ocirc;n Ma Thuột, người mua h&agrave;ng) cho biết, với c&aacute;c sản phẩm đồ gỗ được b&agrave;y b&aacute;n n&agrave;y c&oacute; kh&aacute; nhiều loại được chủ cửa h&agrave;ng h&eacute;t gi&aacute; kh&aacute; cao n&ecirc;n người mua h&agrave;ng bao giờ cũng trả gi&aacute; để thỏa thuận được mức mua h&agrave;ng ph&ugrave; hợp. \"T&ocirc;i thấy v&agrave;o chiều ng&agrave;y 30 tết h&agrave;ng năm c&aacute;c sản phẩm lu&ocirc;n được rao b&aacute;n với gi&aacute; rẻ hơn rất nhiều n&ecirc;n sẽ chờ v&agrave;o ng&agrave;y h&ocirc;m đ&oacute; mới quyết định mua\", anh Ho&agrave;ng cho hay.</p>\r\n<p>Được biết, hội chợ hoa xu&acirc;n TP. Bu&ocirc;n Ma Thuột được diễn ra từ ng&agrave;y 2/2 &ndash; 15/2 (chiều 30 tết) tại khu vực Quảng trường 10.3 v&agrave; 8 tuyến đường trung t&acirc;m của th&agrave;nh phố.</p>\r\n<p style=\"text-align: right;\"><em>Nguồn: dantri.com.vn</em></p>\r\n<p>&nbsp;</p>', 1, '2019-01-07 02:12:35', '2019-05-31 10:14:15'),
(7, 'Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi sdasdsdfsdsdfsdfsdf', 61, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', '<p>sdfds</p>', '<p>sdfsdfsdf</p>', 4, '2019-05-31 10:39:26', '2019-05-31 22:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_categories`
--

INSERT INTO `post_categories` (`id`, `title`, `description`, `slug_id`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Huawei MediaPad M2 Nhật', 'ádasdas fgh fghfg', 1, 0, '2019-01-03 00:21:47', '2019-02-18 23:49:39'),
(4, 'Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi', 'sadasd', 62, 1, '2019-05-31 10:45:43', '2019-05-31 10:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` int(1) DEFAULT '0',
  `slug_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `productcate_id` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `featured`, `slug_id`, `price`, `thumbnail`, `description`, `body`, `productcate_id`, `created_at`, `updated_at`) VALUES
(3, 'Tranh tứ quý gỗ hương 88x42x3cm', 1, 10, 0, '/photos/1/Tranh tứ quý gỗ hương 88x42x3cm/do-go-my-nghe-thien-minh-tranh-tu-quy-go-huong.jpg', '<p>rgtret rdegtret</p>', '<p><img src=\"/photos/1/Tranh tứ qu&yacute; gỗ hương 88x42x3cm/do-go-my-nghe-thien-minh-tranh-tu-quy-go-huong-1.png\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-06 23:45:45', '2019-02-19 00:54:38'),
(4, 'Tượng Tam Đa gỗ hương 48x17x15cm', 1, 11, 0, '/photos/1/Tượng Tam Đa gỗ hương 48x17x15cm/thumbnail_2_99976ff80dcd7483a54ad85e1808319f.jpg', '<p>tdhtdh tdyytdhtd</p>', '<p><img src=\"/photos/1/Tượng Tam Đa gỗ hương 48x17x15cm/tuong tam da go huong (4).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Tam Đa gỗ hương 48x17x15cm/tuong tam da go huong (3).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Tam Đa gỗ hương 48x17x15cm/tuong tam da go huong (2).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Tam Đa gỗ hương 48x17x15cm/tuong tam da go huong (1).jpg\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-07 00:24:52', '2019-02-19 00:54:31'),
(5, 'Tượng Tam Thánh gỗ hương 101x60x50cm', 1, 15, 0, '/photos/1/thumbnail_2_8a9675a1f3982f80671eeebb138c8f6b.jpg', '<p>ghjgf tfghfth fhtfhgf</p>', '<p><img src=\"/photos/1/tuong tam thanh go huong 3.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/tuong tam thanh go huong.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/tuong tam thanh go huong 1.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/tuong tam thanh go huong 2.jpg\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-07 03:55:17', '2019-02-19 00:54:23'),
(6, 'Tượng Di Lặc chúc phúc huyết long 30x49x28cm', 1, 19, 0, '/photos/1/Tượng Di Lặc chúc phúc huyết long 30x49x28cm/do-go-my-nghe-thien-minh-tuong-di-lac-chuc-phuc-huyet-long.jpg', '<p>hkjdahsdkjhkjda</p>', '<p><img src=\"/photos/1/Tượng Di Lặc ch&uacute;c ph&uacute;c huyết long 30x49x28cm/tuong di lac chuc phuc huyet long 2.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Di Lặc ch&uacute;c ph&uacute;c huyết long 30x49x28cm/tuong di lac chuc phuc huyet long.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Di Lặc ch&uacute;c ph&uacute;c huyết long 30x49x28cm/tuong di lac chuc phuc huyet long 1.jpg\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-07 06:38:46', '2019-02-19 00:54:15'),
(7, 'Lộc bình gỗ đinh hương 180x50cm', 1, 20, 0, '/photos/1/Lộc bình gỗ đinh hương 180x50cm/do-go-my-nghe-thien-minh-luc-binh-go-dinh-huong.jpg', '<p>đ&acirc;sasd</p>', '<p><img src=\"/photos/1/Lộc b&igrave;nh gỗ đinh hương 180x50cm/loc-binh-go-dinh-huong.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Lộc b&igrave;nh gỗ đinh hương 180x50cm/loc-binh-go-dinh-huong-1.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Lộc b&igrave;nh gỗ đinh hương 180x50cm/loc-binh-go-dinh-huong-2.jpg\" alt=\"\" width=\"100%\" /></p>', 2, '2019-01-07 06:43:41', '2019-05-31 22:34:27'),
(8, 'Tranh cá chép hóa rồng gỗ bách xanh Sơn La', 1, 21, 0, '/photos/1/Tranh cá chép hóa rồng gỗ bách xanh Sơn La/do-go-my-nghe-thien-minh-tranh-ca-chep-hoa-rong.jpg', '<p>hh</p>', '<p><img src=\"/photos/1/Tranh c&aacute; ch&eacute;p h&oacute;a rồng gỗ b&aacute;ch xanh Sơn La/tranh ca chep hoa rong go bach xanh son la (1).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tranh c&aacute; ch&eacute;p h&oacute;a rồng gỗ b&aacute;ch xanh Sơn La/tranh ca chep hoa rong go bach xanh son la (4).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tranh c&aacute; ch&eacute;p h&oacute;a rồng gỗ b&aacute;ch xanh Sơn La/tranh ca chep hoa rong go bach xanh son la (3).jpg\" alt=\"\" width=\"100%\" /></p>', 2, '2019-01-07 06:48:37', '2019-05-31 22:31:16'),
(9, 'Bầu bí nu hương 40x40x15cm', 1, 22, 0, '/photos/1/Bầu bí nu hương 40x40x15cm/do-go-my-nghe-thien-minh-bau-bi-lu-huong.jpg', '<p>g</p>', '<p><img src=\"/photos/1/Bầu b&iacute; nu hương 40x40x15cm/bau bi go nu huong 1(1).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Bầu b&iacute; nu hương 40x40x15cm/bau bi go nu huong 2(1).jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Bầu b&iacute; nu hương 40x40x15cm/bau bi go nu huong 5.jpg\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-07 06:53:15', '2019-02-19 00:53:59'),
(10, 'Tượng Song Mã gỗ lũa bách xanh 86x62x41cm', 1, 23, 0, '/photos/1/Tượng Song Mã gỗ lũa bách xanh 86x62x41cm/do-go-my-nghe-thien-minh--tuong-song-ma-go-bach-xanh.jpg', '<p>h</p>', '<p><img src=\"/photos/1/Tượng Song M&atilde; gỗ lũa b&aacute;ch xanh 86x62x41cm/tuong-song-ma-go-lua-bach-xanh.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Song M&atilde; gỗ lũa b&aacute;ch xanh 86x62x41cm/tuong-song-ma-go-lua-bach-xanh-2.jpg\" alt=\"\" width=\"100%\" /></p>\r\n<p><img src=\"/photos/1/Tượng Song M&atilde; gỗ lũa b&aacute;ch xanh 86x62x41cm/tuong-song-ma-go-lua-bach-xanh-5.jpg\" alt=\"\" width=\"100%\" /></p>', 11, '2019-01-07 06:57:20', '2019-02-19 00:53:38'),
(15, 'Sập nu nghiến 4 mặt', 0, 57, 0, '/photos/1/Sập nu nghiến 4 mặt/sap-nu-nghien-5.png', '<p>aa</p>', '<p><img src=\"/photos/1/Sập nu nghiến 4 mặt/sap-nu-nghien-2.png\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/Sập nu nghiến 4 mặt/sap-nu-nghien-5.png\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/Sập nu nghiến 4 mặt/sap-nu-nghien-3.png\" alt=\"\" width=\"100%\" /></p>', 12, '2019-02-19 01:15:54', '2019-02-19 01:15:54'),
(16, 'Bộ Bàn Ăn Nguyên Tấm', NULL, 58, 0, '/photos/1/Bộ bàn ăn nguyên tấm/bo-ban-ghe-an-mot-la1.png', '<p>k</p>', '<p><img src=\"/photos/1/Bộ b&agrave;n ăn nguy&ecirc;n tấm/bo-ban-ghe-an-mot-la3.png\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/Bộ b&agrave;n ăn nguy&ecirc;n tấm/bo-ban-ghe-an-mot-la1.png\" alt=\"\" width=\"100%\" /></p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/photos/1/Bộ b&agrave;n ăn nguy&ecirc;n tấm/bo-ban-ghe-an-mot-la2.png\" alt=\"\" width=\"100%\" /></p>', 11, '2019-02-19 01:24:45', '2019-02-19 01:24:45'),
(17, 'Bộ Bàn Ghế Hoa Lan Tây', 0, 59, 0, '/photos/1/c10-15179725083532049766442.jpg', '<p>lsdfsd</p>', '<p>sdfsd</p>\r\n<p><img src=\"/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg\" alt=\"\" /></p>', 11, '2019-02-19 01:30:15', '2019-05-31 23:50:40'),
(18, 'Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi sdasd', 1, 65, 0, '/photos/1/40607165_1520085818136884_5273189253495390208_n.jpg', '<p>ghjghj</p>', '<p>ghjghjghj</p>', 11, '2019-05-31 23:52:33', '2019-05-31 23:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `title`, `description`, `slug_id`, `parent_id`, `created_at`, `updated_at`) VALUES
(2, 'do go my nghe', 'sdfsdfsdfsdf', 9, 0, '2019-01-06 23:39:49', '2019-01-08 23:02:00'),
(11, 'Nôi thất phòng khách sdfsdf', 'Nội Thất Phòng khách', 55, 2, '2019-02-19 00:47:07', '2019-05-31 22:36:05'),
(12, 'Nội thất phòng ngủ', 'Nội Thất Phòng khách', 56, 2, '2019-02-19 01:10:10', '2019-02-19 01:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'Admin', 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`identifier`, `instance`, `content`, `created_at`, `updated_at`, `message`) VALUES
('username', 'wishlist', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slugs`
--

CREATE TABLE `slugs` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slugs`
--

INSERT INTO `slugs` (`id`, `slug`, `type`, `created_at`, `updated_at`) VALUES
(1, 'ddrgtdr', 'postcate', '2019-01-03 00:21:47', '2019-01-06 23:32:37'),
(2, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-te-rwetretretret', 'post', '2019-01-03 00:43:00', '2019-01-03 00:43:18'),
(3, 'huawei-mediapad-m2-nhat-8-dthdth', 'productcate', '2019-01-03 00:54:14', '2019-01-06 23:32:20'),
(6, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-te-dwsfsdf', 'product', '2019-01-03 01:01:41', '2019-01-03 01:01:41'),
(7, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-te-dsfsdfsdf', 'product', '2019-01-03 01:10:28', '2019-01-03 01:10:28'),
(8, 'vi-sao-cay-go-tra-gia-1-trieu-usd-khong-ban-o-thanh-hoa-lai-duoc-xem-la-bau-vat', 'post', '2019-01-06 22:26:09', '2019-01-08 19:29:58'),
(9, 'huawei', 'productcate', '2019-01-06 23:39:49', '2019-01-06 23:41:26'),
(10, 'tranh-tu-quy-go-huong-88x42x3cm', 'product', '2019-01-06 23:45:45', '2019-01-07 06:33:38'),
(11, 'tuong-tam-da-go-huong-48x17x15cm', 'product', '2019-01-07 00:24:52', '2019-01-07 06:25:24'),
(12, 'dai-gia-“san”-do-go-my-nghe-vai-tram-trieu-o-cho-hoa-xuan-tphcm', 'post', '2019-01-07 02:09:53', '2019-01-08 19:22:33'),
(13, 'do-go-tien-ti-xuong-pho-nguoi-mua-tram-tro', 'post', '2019-01-07 02:12:15', '2019-01-08 18:59:20'),
(14, 'do-go-my-nghe-tinh-xao-gia-“khung”-tai-cho-xuan-pho-n', 'post', '2019-01-07 02:12:35', '2019-05-31 10:39:08'),
(15, 'tuong-tam-thanh-go-huong-101x60x50cm', 'product', '2019-01-07 03:55:17', '2019-01-07 06:14:48'),
(16, 'huawei-mediapad-m2-nhat-8-inch', 'page', '2019-01-07 05:43:02', '2019-01-07 05:43:50'),
(17, 'gioi-thieu', 'page', '2019-01-07 05:45:02', '2019-01-08 23:26:56'),
(18, 'lien-he', 'page', '2019-01-07 05:45:29', '2019-01-07 05:45:29'),
(19, 'tuong-di-lac-chuc-phuc-huyet-long-30x49x28cm', 'product', '2019-01-07 06:38:46', '2019-01-07 06:38:46'),
(20, 'loc-binh-go-dinh-huong-180x50cm', 'product', '2019-01-07 06:43:41', '2019-01-07 06:43:41'),
(21, 'tranh-ca-chep-hoa-rong-go-bach-xanh-son-la', 'product', '2019-01-07 06:48:37', '2019-01-07 06:48:37'),
(22, 'bau-bi-nu-huong-40x40x15cm', 'product', '2019-01-07 06:53:15', '2019-01-07 06:53:15'),
(23, 'tuong-song-ma-go-lua-bach-xanh-86x62x41cm', 'product', '2019-01-07 06:57:20', '2019-01-07 06:57:20'),
(28, 'thu-phat-hihi', 'postcate', '2019-01-09 09:54:38', '2019-01-09 09:54:38'),
(33, 'vdvsdv-fdfdf-dffd', 'productcate', '2019-02-18 05:44:09', '2019-02-18 05:44:09'),
(34, 'vdvsdv-fdfdf-dffdzxzx-czxc', 'productcate', '2019-02-18 05:45:05', '2019-02-18 05:45:05'),
(35, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-tesdasdasd', 'productcate', '2019-02-18 05:49:05', '2019-02-18 05:49:05'),
(36, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-te-dwsfsdf-vgbv-bv', 'productcate', '2019-02-18 05:58:12', '2019-02-18 05:58:12'),
(37, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-tesdasdasd-cvcv', 'productcate', '2019-02-18 05:59:05', '2019-02-18 05:59:05'),
(39, 'vxv-dfgvfg', 'productcate', '2019-02-18 06:12:50', '2019-02-18 06:12:50'),
(40, 'asdasd-fgbdgf-gdgd', 'productcate', '2019-02-18 06:14:55', '2019-02-18 06:14:55'),
(41, 'asdasd-fgbdgf-gdgd-dfgdf-dfdgdf-fdgdfg', 'productcate', '2019-02-18 06:15:14', '2019-02-18 06:15:14'),
(42, 'sfvsdfv-dfvdsfv-sdfvsdfsd', 'productcate', '2019-02-18 06:16:34', '2019-02-18 06:16:34'),
(43, 'asdasd-fgbdgf-gdgd-dfgdf-dfdgdf-fdgdfgghfgh-fghfg', 'productcate', '2019-02-18 06:17:21', '2019-02-18 06:17:21'),
(44, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-te-34r24r234r', 'productcate', '2019-02-18 06:23:18', '2019-02-18 06:23:18'),
(45, 'huawei-mediapad-m2-nhat-8-inch-dtab-d-02h-chay-rom-quoc-tesdasdasdsxvs-dsfv324r23sdcfsd', 'productcate', '2019-02-18 06:25:04', '2019-02-18 06:25:04'),
(46, 'fdsf-dsfsdf-sdfdsf-sdfsdf', 'productcate', '2019-02-18 23:06:35', '2019-02-18 23:06:35'),
(49, 'fgdgv', 'product', '2019-02-18 23:14:55', '2019-02-18 23:14:55'),
(50, 'fgdgv-gfvhfgh', 'product', '2019-02-18 23:15:48', '2019-02-18 23:15:48'),
(55, 'noi-that-phong-khach-sdfsdf', 'productcate', '2019-02-19 00:47:07', '2019-05-31 22:36:05'),
(56, 'noi-that-phong-ngu', 'productcate', '2019-02-19 01:10:10', '2019-02-19 01:10:10'),
(57, 'sap-nu-nghien-4-mat', 'product', '2019-02-19 01:15:54', '2019-02-19 01:15:54'),
(58, 'bo-ban-an-nguyen-tam', 'product', '2019-02-19 01:24:45', '2019-02-19 01:24:45'),
(59, 'bo-ban-ghe-hoa-lan-tay', 'product', '2019-02-19 01:30:15', '2019-02-19 01:30:15'),
(61, 'do-go-my-nghe-tinh-xao-gia-“khung”-tai-cho-xuan-pho-nui-sdasdsdfsdsdfsdfsdf', 'post', '2019-05-31 10:39:26', '2019-05-31 10:39:26'),
(62, 'do-go-my-nghe-tinh-xao-gia-“khung”-tai-cho-xuan-pho-nui', 'postcate', '2019-05-31 10:45:43', '2019-05-31 10:56:37'),
(65, 'do-go-my-nghe-tinh-xao-gia-“khung”-tai-cho-xuan-pho-nui-sdasd', 'product', '2019-05-31 23:52:33', '2019-05-31 23:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Vũ Văn Tiến 1', 'tien2vv@gmail.com', '$2y$10$ZLclU8OsRcH1VPtuWIVcQ.RP8eP/18LFbcSCMySAiDJkiIepcXzvW', 'WAdAbr6k5B8zmRiiqcDjeiHN3m0ofzZMHRRBiO60nGG8BM1weegJtwTRFriz', '2018-12-21 08:11:51', '2018-12-21 08:24:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menus`
--
ALTER TABLE `admin_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu_items`
--
ALTER TABLE `admin_menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_menu_items_menu_foreign` (`menu`);

--
-- Indexes for table `cart_contacts`
--
ALTER TABLE `cart_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_slug_id_foreign` (`slug_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_slug_id_foreign` (`slug_id`),
  ADD KEY `posts_postcate_id_foreign` (`postcate_id`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_categories_slug_id_foreign` (`slug_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_slug_id_foreign` (`slug_id`),
  ADD KEY `products_productcate_id_foreign` (`productcate_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories_slug_id_foreign` (`slug_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `slugs`
--
ALTER TABLE `slugs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menus`
--
ALTER TABLE `admin_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admin_menu_items`
--
ALTER TABLE `admin_menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cart_contacts`
--
ALTER TABLE `cart_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slugs`
--
ALTER TABLE `slugs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_menu_items`
--
ALTER TABLE `admin_menu_items`
  ADD CONSTRAINT `admin_menu_items_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `admin_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_slug_id_foreign` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_postcate_id_foreign` FOREIGN KEY (`postcate_id`) REFERENCES `post_categories` (`id`),
  ADD CONSTRAINT `posts_slug_id_foreign` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`id`);

--
-- Constraints for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD CONSTRAINT `post_categories_slug_id_foreign` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_productcate_id_foreign` FOREIGN KEY (`productcate_id`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `products_slug_id_foreign` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`id`);

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_slug_id_foreign` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
